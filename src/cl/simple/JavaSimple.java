
package cl.simple;

import org.apache.log4j.Logger;


public class JavaSimple {
    
    final static Logger LOGGER = Logger.getLogger(JavaSimple.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        LOGGER.debug("*****************************");
        LOGGER.debug("Java Simple");
        LOGGER.debug("*****************************");

    }
    
}
